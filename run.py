#!/bin/python3

import argparse
import csv
import multiprocessing
import os
import re
import signal
import subprocess
import sys
import threading
from dataclasses import dataclass
from functools import partial
from queue import Queue

float_regex = r'[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?'
higgs_regex = re.compile(r'h\s+(' + float_regex + ') (' + float_regex + ')')
lsp_regex = re.compile(r'Masses of odd sector Particles:\n([^\s]+)\s+: ([^\s]+)\s+=\s+(' + float_regex + ')')
CDM_proton_regex = re.compile(r'proton\s+SI\s+(' + float_regex + ')\s+SD\s(' + float_regex + ')')
CDM_neutron_regex = re.compile(r'neutron\s+SI\s+(' + float_regex + ')\s+SD\s(' + float_regex + ')')
acs_regex = re.compile(r'annihilation cross section (' + float_regex + ')')
relic_density_regex = re.compile(r'Xf=(' + float_regex + ')\s+Omega=(' + float_regex + ')')


@dataclass
class RunResults:
    """..."""
    m0: str
    mhf: str
    a0: str
    tanB: str
    h_mass: str
    h_width: str
    lsp_particle: str
    lsp_mass: str
    CDM_cross_section_proton: str
    CDM_cross_section_neutron: str
    ACS: str
    Xf: str
    Omega: str

    @staticmethod
    def rowHeaders():
        return [
            'm0',
            "mhf",
            "a0",
            "tanB",
            "h_mass",
            "h_width",
            "lsp_particle",
            "lsp_mass",
            "CDM Proton",
            "CDM Neutron",
            "ACS",
            "Xf",
            "Omega",
        ]

    def getRow(self):
        return [
            self.m0,
            self.mhf,
            self.a0,
            self.tanB,
            self.h_mass,
            self.h_width,
            self.lsp_particle,
            self.lsp_mass,
            self.CDM_cross_section_proton,
            self.CDM_cross_section_neutron,
            self.ACS,
            self.Xf,
            self.Omega,
        ]


def run_process(args, queue, main_location='./'):
    """
    Run the process and process the output.

    :param args:
        A list of arguments to pass the the micromegas process

    :return:
    """

    # [./main] + [m0 m12 -m12 30]
    # [./main m0 m12 -m12 30]
    proc = subprocess.Popen([main_location + 'main'] + args,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE)
    output, err = proc.communicate()
    output = output.decode('latin1')
    err = err.decode('latin1')
    if err != '':
        # Send output to the error so it can be logged.
        print(err, file=sys.stderr)
        print(output, file=sys.stderr)
    else:
        higgs = re.search(higgs_regex, output)
        lsp = re.search(lsp_regex, output)
        cdm_proton = re.search(CDM_proton_regex, output)
        cdm_neutron = re.search(CDM_neutron_regex, output)
        acs = re.search(acs_regex, output)
        relic_density = re.search(relic_density_regex, output)

        if higgs and lsp and cdm_proton and cdm_neutron and acs and relic_density:
            r = RunResults(args[0], args[1], args[2], args[3],
                           higgs.group(1), higgs.group(3),
                           lsp.group(2), lsp.group(3),
                           cdm_proton.group(1), cdm_neutron.group(1),
                           acs.group(1),
                           relic_density.group(1), relic_density.group(3))
            queue.put(r)
            return r
        else:
            # Send output to the error so it can be logged.
            print(err, file=sys.stderr)
            print(output, file=sys.stderr)


def threaded_writer(q):
    """Listens for rows on the queue and writes them to our output file."""

    with open('run_data.csv', mode='w') as data_file:
        report_writer = csv.writer(data_file)
        report_writer.writerow(RunResults.rowHeaders())

        while 1:
            message = q.get()
            if message is None:
                data_file.flush()
                break
            print(message.getRow())
            report_writer.writerow(message.getRow())


def initialize_run_process():
    """Setup per process run directories"""
    wd = os.getcwd() + '/work/tmp/' + str(os.getpid())
    try:
        os.chdir(wd)
    except FileNotFoundError:
        os.mkdir(wd)
        os.chdir(wd)
    pass


def get_writer_thread(queue):
    """Get a thread writer thread"""
    writer_thread = threading.Thread(target=threaded_writer, args=[queue])
    writer_thread.start()
    return writer_thread


def run(arg_list):
    queue = Queue()
    writer_thread = get_writer_thread(queue)
    for process_args in arg_list:
        run_process(process_args, queue)
    queue.put(None)
    writer_thread.join()


def run_threaded(arg_list):
    """Multi-threaded process runner."""
    # Multithreading based on https://stackoverflow.com/a/35134329/988535
    original_sigint_handler = signal.signal(signal.SIGINT, signal.SIG_IGN)
    manager = multiprocessing.Manager()
    queue = manager.Queue()
    pool = multiprocessing.Pool(round(multiprocessing.cpu_count() * 1.5), initialize_run_process)
    signal.signal(signal.SIGINT, original_sigint_handler)

    writer_thread = get_writer_thread(queue)

    try:
        # Actually run the processes.
        res = pool.map_async(partial(run_process, queue=queue, main_location=os.getcwd() + '/'), arg_list)
        res.get(9999999)
    except KeyboardInterrupt:
        pool.terminate()
    else:
        pool.close()
    queue.put(None)
    writer_thread.join()
    pool.join()


def main(m_lower=6000, m_upper=6750, mhf_lower=1000, mhf_upper=2000, m0_step=5, mhf_step=2, thread=False):
    # increment the upper cut off so the range is inclusive.
    m_upper += m0_step
    mhf_upper += mhf_step
    # Build a list of all possible M0 and MHF values.
    args_list = []
    for M0 in range(m_lower, m_upper, m0_step):
        for MHF in range(mhf_lower, mhf_upper, mhf_step):
            args_list.append([str(M0), str(MHF), str(-MHF), "30"])
    if thread:
        run_threaded(args_list)
    else:
        run(args_list)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run micromegas over a range of values.')
    parser.add_argument('--thread', action='store_true',
                        help='Run values in concurrent processes')
    parser.add_argument('--m_lower', default=6000, type=int,
                        help='Lower M0 value')
    parser.add_argument('--m_upper', default=6750, type=int,
                        help='Upper M0 value')
    parser.add_argument('--mhf_lower', default=1000, type=int,
                        help='M 1/2 lower value')
    parser.add_argument('--mhf_upper', default=2000, type=int,
                        help='M 1/2 upper value')
    parser.add_argument('--m0_step', default=5, type=int,
                        help='M0 step size')
    parser.add_argument('--mhf_step', default=2, type=int,
                        help='M 1/2 step size')
    args = parser.parse_args()

    if len(sys.argv) == 5:
        args = sys.argv
        main(int(args[1]), int(args[2]), int(args[3]), int(args[4]))
    else:
        main(**vars(args))
