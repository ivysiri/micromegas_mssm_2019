
# Define our main so we don't have to specify it.
main=main.cpp

# Run the model makefile
include ../include/modelMakefile

specialClean: 
	rm -f lanhep/*.mdl  lanhep/masses.chk 
	rm -f suspect2_lha.in suspect2_lha.out suspect2.out nngg.out nngg.in  output.flh
