#!/bin/bash

if [ ! -f "./main" ]; then
  echo "Building main"
  # Redirect everything to dev null... the errors are long and generally impossible to parse so...
  make main=main.cpp &> /dev/null
fi

if [ ! -f "../Packages/smodels-v1.1.1/smodelsTools.py" ]; then
   cd ../Packages || exit;
   make -f SMODELS.make
   cd -
fi
echo "Running process"
pipenv run python ./run.py $@
